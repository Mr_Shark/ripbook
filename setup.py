import os

from setuptools import setup


def read(fname):
        return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="ripbook",
    version="0.9",
    author="jens persson",
    author_email="jens@persson.cx",
    description="audio ripper wrapper for audio books",
    license="GPL-3",
    keywords="audio books ripper cd",
    url="https://bitbucket.org/Mr_Shark/ripbook/",
    packages=['ripbook', 'test'],
    entry_points={
        'console_scripts': [
            'ripbook = ripbook.commands:ripbook',
        ],
    },
    long_description=read('README'),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Topic :: Multimedia :: Sound/Audio :: CD Audio :: CD Ripping",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
