#!/usr/bin/env python

"""
  ripbook - audio ripper wrapper for audio books

  Purpose:
  ripbook handles the nitty gritty of riping an audiobook from many cds
  to ogg-files
"""

import os
import sys

from ripbook.Cd import read_cd
from ripbook.Settings import read_settings
from ripbook.CommandParser import dialog, QUIT, NEXT


scriptname = os.path.basename(sys.argv[0])
__version__ = "0.1"
__id__ = ""


def ripbook():
    """
    Main processing
    """
    settings = read_settings()
    (command, data) = dialog(cdnum=settings.cdnum)
    while command != QUIT:
        if command == NEXT:
            cd = read_cd(settings)
            for track in cd.tracks:
                print "Ripping track:", track.num, "of", cd.number_of_tracks
                track.rip()
                print "Encoding track:", track.num, "of", cd.number_of_tracks
                track.encode()
            cd.finalize()
            settings.cdnum += 1
        (command, data) = dialog(cdnum=settings.cdnum)

    return 0


if __name__ == '__main__' or __name__ == sys.argv[0]:
    try:
        sys.exit(ripbook())
    except KeyboardInterrupt, error:
        print "[%s]  Interrupted!" % scriptname
