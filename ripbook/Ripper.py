from subprocess import check_output, CalledProcessError

CDPARANOIA = "cdparanoia"
DUMMY = "dummy"
ICEDAX = "icedax"

all_rippers = [CDPARANOIA, ICEDAX, DUMMY, ]


class Ripper(object):
    def __init__(self, device):
        self.to_file_ext = "wav"
        self.device = device


class CdparanoiaRipper(Ripper):
    def rip(self, track_no, to_file):
        """Copy the track from the cd to disk as a wav file"""
        check_output(["cdparanoia", "-d", self.device, str(track_no), to_file])

    def number_of_tracks(self):
        diskid = check_output(["cd-discid", self.device])
        return int(diskid.split(" ")[1])

    def eject(self):
        try:
            check_output(["eject", self.device])
        except CalledProcessError:
            print "CD did not eject, please do it manually"
            pass


class IcedaxRipper(Ripper):
    def rip(self, track_no, to_file):
        """Copy the track from the cd to disk as a wav file"""
        check_output(["icedax",
                      "-device", self.device,
                      "--no-infofile", "--gui",
                      "-t", str(track_no), to_file])

    def number_of_tracks(self):
        diskid = check_output(["cd-discid", self.device])
        return int(diskid.split(" ")[1])

    def eject(self):
        check_output(["eject", self.device])


class DummyRipper(Ripper):
    def rip(self, track_no, to_file):
        open(to_file, "w").close()

    def number_of_tracks(self):
        return 10

    def eject(self):
        pass


class UnknownRipper(Exception):
    pass


def ripper_factory(variant, device=None):
    if variant == CDPARANOIA:
        return CdparanoiaRipper(device)
    elif variant == ICEDAX:
        return IcedaxRipper(device)
    elif variant == DUMMY:
        return DummyRipper(device)
    else:
        raise UnknownRipper()
