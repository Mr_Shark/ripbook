import os


class Cd(object):
    """Info about a CD"""

    def __init__(self, settings):
        self.settings = settings

        self.number_of_tracks = settings.ripper.number_of_tracks()
        self.tracks = [Track(n + 1, settings) for n in range(self.number_of_tracks)]

    def finalize(self):
        """Fix up after a cd is fully riped"""
        self.settings.ripper.eject()
        self.settings.starttrack += self.number_of_tracks


class Track(object):
    """One cd track and its riped files"""
    # TODO: support more rippers/encoders
    def __init__(self, num, settings):
        self.num = num
        self.globaltrack = settings.starttrack + self.num - 1
        self.basename = os.path.join(settings.directory, "%03i." % self.globaltrack)
        self.encoder = settings.encoder
        self.ripper = settings.ripper

    @property
    def encode_file(self):
        """Produce the final name for this track"""
        ext = self.encoder.to_file_ext
        return self.basename + ext

    @property
    def rip_file(self):
        """Produce the name of the ripped file"""
        ext = self.ripper.to_file_ext
        return self.basename + ext

    def rip(self):
        """Copy the track from the cd to disk as a wav file"""
        self.ripper.rip(self.num, self.rip_file)

    def encode(self):
        """Encode the wav file to ogg"""
        self.encoder.encode(self.rip_file, self.encode_file,
                            self.globaltrack)
        os.unlink(self.rip_file)


def read_cd(settings):
    """
    Returns info about the current cd
    """
    return Cd(settings)
