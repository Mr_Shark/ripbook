from subprocess import check_output

OGGENC = "oggenc"
DUMMY = "dummy"

all_encoders = [OGGENC, DUMMY, ]


class UnknownEncoder(Exception):
    pass


class Encoder(object):
    def __init__(self, title, author):
        self.to_file_ext = "ogg"
        self.title = title
        self.author = author


class OggencEncoder(Encoder):
    def encode(self, from_file, to_file, track_no):
        """Encode the wav file to ogg"""
        check_output(["oggenc",
                      "--output=" + to_file,
                      "--album=" + self.title,
                      "--artist=" + self.author,
                      "--tracknum=" + str(track_no),
                      from_file, ])


class DummyEncoder(Encoder):
    def encode(self, from_file, to_file, track_no):
        open(to_file, "w").close()


def encoder_factory(encoder_type, title, author):
    if encoder_type == OGGENC:
        return OggencEncoder(title, author)
    elif encoder_type == DUMMY:
        return DummyEncoder(title, author)
    else:
        raise UnknownEncoder()
