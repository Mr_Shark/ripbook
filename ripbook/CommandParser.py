#!/usr/bin/env python

"""
  ripbook.CommandParser

  Purpose:
  Handle the interaction with the user
"""

QUIT = 0
NEXT = 1


def dialog(**state):
    """
    Ask the user to insert the next cd, or terminate.
    Returns False if the user terminates
    """
    # TODO: Allow entering the disk number (and track?)
    print "Insert next cd (no {0}) and hit enter (or q first to quit)".format(state["cdnum"])
    answer = raw_input(">>")
    if answer == "q":
        return (QUIT, None)
    else:
        return (NEXT, None)
