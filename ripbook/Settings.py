import argparse
import sys
import os
import os.path

from ripbook import Encoder
from ripbook import Ripper


class Settings(object):
    """
    A collection of settings for ripping a audio book.
    """
    # TODO: Store changes automatically
    def __init__(self, argv):
        parser = argparse.ArgumentParser(description='Audio book ripper')
        parser.add_argument('--debug', dest='debug_level',
                            type=int, default=0, help='Debug level, higher is more info')

        parser.add_argument('--author', dest='author',
                            type=str, default="Unknown author", help='The author of the book')

        parser.add_argument('--title', dest='title',
                            type=str, default="Unknown title", help='The title of the book')

        parser.add_argument('-o', '--outputdir', dest='directory',
                            type=str, help='Where to put the riped files. Default is <author>/<title>')

        parser.add_argument('--starttrack', dest='starttrack',
                            type=int, default=1, help='Track number to start from')

        parser.add_argument('--cdnumber', dest='cdnumber',
                            type=int, default=1, help='Cd number to start from')

        parser.add_argument('--device', dest='device',
                            type=str, default="/dev/cdrom", help='Device to rip from')

        parser.add_argument('--ripper', dest='ripper', choices=Ripper.all_rippers,
                            type=str, default=Ripper.ICEDAX, help='Select the ripper to use')

        parser.add_argument('--encoder', dest='encoder', choices=Encoder.all_encoders,
                            type=str, default=Encoder.OGGENC, help='Select the encoder to use')

        args = parser.parse_args(argv)

        self.device = args.device
        self.starttrack = args.starttrack
        self.cdnum = args.cdnumber
        self.author = args.author
        self.title = args.title
        if args.directory:
            self.directory = args.directory
        else:
            self.directory = os.path.join(".", self.author, self.title)
        try:
            os.makedirs(self.directory)
        except OSError:
            pass

        self.ripper = Ripper.ripper_factory(args.ripper, self.device)
        self.encoder = Encoder.encoder_factory(args.encoder, self.title, self.author)


def read_settings():
    """
    Read the current settings from the disk and
    Initialize the environment and parse options
    """
    # TODO: read settings file
    return Settings(sys.argv[1:])
