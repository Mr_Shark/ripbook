from ripbook import Encoder

import unittest


class EncoderTests(unittest.TestCase):
    def test_factory(self):
        oggenc_encoder = Encoder.encoder_factory(Encoder.OGGENC, "title", "author")
        dummy_encoder = Encoder.encoder_factory(Encoder.DUMMY, "title", "author")

        self.assertEqual(type(oggenc_encoder), Encoder.OggencEncoder)
        self.assertEqual(type(dummy_encoder), Encoder.DummyEncoder)
        self.assertRaises(Encoder.UnknownEncoder, Encoder.encoder_factory, "no_such_encoder", "title", "author")


if __name__ == '__main__':
    unittest.main()
