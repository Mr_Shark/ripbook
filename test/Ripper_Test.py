import unittest

from ripbook import Ripper


class RipperTests(unittest.TestCase):
    def test_factory(self):
        cdparanoia_ripper = Ripper.ripper_factory(Ripper.CDPARANOIA)
        dummy_ripper = Ripper.ripper_factory(Ripper.DUMMY)

        self.assertEqual(type(cdparanoia_ripper), Ripper.CdparanoiaRipper)
        self.assertEqual(type(dummy_ripper), Ripper.DummyRipper)
        self.assertRaises(Ripper.UnknownRipper, Ripper.ripper_factory, "no_such_ripper")


if __name__ == '__main__':
    unittest.main()
